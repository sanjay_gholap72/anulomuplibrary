package com.anulom.service.anulomuploaddoc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ImageChooser extends AppCompatActivity implements View.OnClickListener {
    private  Button openCustomGallery,btnUpload;
    private  GridView selectedImageGridView;

    private static final int CustomGallerySelectId = 1;//Set Intent Id
    public static final String CustomGalleryIntentKey = "ImageArray";//Set Intent Key Value
    static final Integer WRITE_EXST = 0x3;

    private DatabaseReference databaseReference;
    private StorageReference storage;

    private String documentDescription,documentID;
    private List<String> selectedImages;
    private Bitmap bitmap;
    private PdfDocument document;
    private PdfDocument.PageInfo pageInfo;
    private PdfDocument.Page page;
    private String name;
    private String pushKey,newPushKey;
    private Uri uploadFile;
    DocumentDetails documentDetails;
    private String userMailID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_chooser);

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            documentID = bundle.getString("documentID");
            documentDescription = bundle.getString("documentDescription");
            userMailID = bundle.getString("EmailID");
        }


        databaseReference = FirebaseDatabase.getInstance().getReference();
        storage= FirebaseStorage.getInstance().getReference();

        openCustomGallery = (Button) findViewById(R.id.openCustomGallery);
        selectedImageGridView = (GridView) findViewById(R.id.selectedImagesGridView);
        btnUpload = (Button) findViewById(R.id.btn_upload);

        openCustomGallery.setOnClickListener(this);
        btnUpload.setOnClickListener(this);
        getSharedImages();
    }


    @Override
    public void onClick(View v)
    {
           if(v == openCustomGallery)
           {
               startActivityForResult(new Intent(ImageChooser.this, CustomGallery_Activity.class), CustomGallerySelectId);
           }
           else if( v ==  btnUpload)
           {
               askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,WRITE_EXST);
               UploadData();
           }
    }

    private void askForPermission(String permission, Integer requestCode)
    {
        if (ContextCompat.checkSelfPermission(ImageChooser.this, permission) != PackageManager.PERMISSION_GRANTED)
        {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ImageChooser.this, permission))
            {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(ImageChooser.this, new String[]{permission}, requestCode);

            }
            else
            {

                ActivityCompat.requestPermissions(ImageChooser.this, new String[]{permission}, requestCode);
            }
        }
        else
        {
            // Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }



    protected void onActivityResult(int requestcode, int resultcode, Intent imagereturnintent)
    {
        super.onActivityResult(requestcode, resultcode, imagereturnintent);
        switch (requestcode)
        {
            case CustomGallerySelectId:
                if (resultcode == RESULT_OK)
                {
                    String imagesArray = imagereturnintent.getStringExtra(CustomGalleryIntentKey);//get Intent data
                    //Convert string array into List by splitting by ',' and substring after '[' and before ']'
                    selectedImages = Arrays.asList(imagesArray.substring(1, imagesArray.length() - 1).split(", "));
                    loadGridView(new ArrayList<String>(selectedImages));//call load gridview method by passing converted list into arrayList

                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED)
        {
            switch (requestCode)
            {
                case 4:
                    Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(imageIntent, 11);
                    break;
                //Camera
                case 5:
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null)
                    {
                        startActivityForResult(takePictureIntent, 12);
                    }
                    break;
                //Accounts

            }

            // Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }

    }


    //Load GridView
    private void loadGridView(ArrayList<String> imagesArray)
    {
        GridView_Adapter adapter = new GridView_Adapter(ImageChooser.this, imagesArray, false);
        selectedImageGridView.setAdapter(adapter);
    }

    //Read Shared Images
    private void getSharedImages()
    {

        //If Intent Action equals then proceed
        if (Intent.ACTION_SEND_MULTIPLE.equals(getIntent().getAction()) && getIntent().hasExtra(Intent.EXTRA_STREAM))
        {
            ArrayList<Parcelable> list = getIntent().getParcelableArrayListExtra(Intent.EXTRA_STREAM);//get Parcelabe list
            ArrayList<String> selectedImages = new ArrayList<>();

            //Loop to all parcelable list
            for (Parcelable parcel : list)
            {
                Uri uri = (Uri) parcel;//get URI
                String sourcepath = getPath(uri);//Get Path of URI
                selectedImages.add(sourcepath);//add images to arraylist
            }
            loadGridView(selectedImages);//call load gridview
        }
    }


    //get actual path of uri
    public String getPath(Uri uri)
    {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    // upload file to firebase database
    private void UploadData()
    {
        Toast.makeText(this, "It takes time to upload image...", Toast.LENGTH_LONG).show();
        int count = selectedImages.size()-1;
        Random r = new Random();
        int i1 = r.nextInt(10000 - 5) + 5;
        name = documentID+"_"+i1+".pdf";

        document = new PdfDocument();
        for(int i=0;i<=count;i++)
        {
            String filePath =(selectedImages.get(i));
            Uri fileUrl = Uri.fromFile(new File(filePath));

            try
            {

                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),fileUrl);
                // image comparess code
                //  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                //bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos);

                // Bitmap bitmap1 = BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()));

                BitmapFactory.Options Options = new BitmapFactory.Options();
                Options.inSampleSize = 2;
                Options.inJustDecodeBounds = false;
                Bitmap bitmap1 = BitmapFactory.decodeFile(filePath, Options);



                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

                Display display = wm.getDefaultDisplay();
                DisplayMetrics displaymetrics = new DisplayMetrics();

                this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                pageInfo = new PdfDocument.PageInfo.Builder(bitmap1.getWidth(), bitmap1.getHeight(), i+1).create();

                page = document.startPage(pageInfo);

                Canvas canvas = page.getCanvas();

                Paint paint = new Paint();
                paint.setColor(Color.parseColor("#ffffff"));
                canvas.drawPaint(paint);

                bitmap1 = Bitmap.createScaledBitmap(bitmap1, bitmap1.getWidth(), bitmap1.getHeight(), true);

                paint.setColor(Color.BLUE);
                canvas.drawBitmap(bitmap1, 0, 15 , null);


                document.finishPage(page);
                // File exportDir1 = new File(Environment.getExternalStorageDirectory(), "/Anulom");
                //File filePath1 = new File(String.valueOf(exportDir1));
                //exportDir1.mkdir();
                try
                {
                    document.writeTo(new FileOutputStream("/storage/emulated/0/"+name));
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                    Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
                }

            }

            catch (Exception e)
            {
                e.printStackTrace();

            }
        }// for loop close

        // close the document
        document.close();

        // close the image to pdf file

        pushKey = databaseReference.push().getKey();
        newPushKey = databaseReference.push().getKey();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String millisInString  = dateFormat.format(new Date());

        uploadFile = Uri.fromFile(new File("/storage/emulated/0/"+name));

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading");
        progressDialog.show();

        StorageReference riversRef = storage.child("Document").child(userMailID).child("UploadDocumentKey").child(newPushKey).child(documentID);

        UploadTask uploadTask = riversRef.putFile(uploadFile);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>()
        {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot)
            {
                progressDialog.dismiss(); //if the upload is successfull  //hiding the progress dialog
                Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();

                Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                task.addOnSuccessListener(new OnSuccessListener<Uri>()
                {
                    @Override
                    public void onSuccess(Uri uri)
                    {
                        String photoLink = uri.toString();

                        documentDetails = new DocumentDetails(documentID,documentDescription,userMailID,millisInString,photoLink);
                        databaseReference.child("Executor").child(userMailID).child("UploadDocument").child(pushKey).setValue(documentDetails);
                        databaseReference.child("User").child("UploadDocumenDetails").child(pushKey).setValue(documentDetails);

                        Intent intent = new Intent(ImageChooser.this,MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });


            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(@NonNull Exception exception)
            {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });



    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(ImageChooser.this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.putExtra("EmailID",userMailID);
        startActivity(intent);

    }
}