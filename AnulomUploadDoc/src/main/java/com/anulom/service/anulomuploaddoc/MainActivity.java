package com.anulom.service.anulomuploaddoc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
     Button btnUpload;
     String userMail;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnUpload = findViewById(R.id.btn_upload);

        btnUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnUpload)
        {
            UploadUI();
        }
    }

    private void UploadUI()
    {
        Intent intent = new Intent(MainActivity.this,HomeActivity.class);
        intent.putExtra("EmailID", userMail);
        startActivity(intent);
    }
}