package com.anulom.service.anulomuploaddoc;

public class DocumentDetails
{
    public String eDocumentID;
    public String eDocumentDescription;
    public String executorName;
    public String date;
    public String pdfUrl;

    public DocumentDetails()
    {

    }

    public DocumentDetails(String eDocumentID, String eDocumentDescription, String executorName, String date, String pdfUrl) {
        this.eDocumentID = eDocumentID;
        this.eDocumentDescription = eDocumentDescription;
        this.executorName = executorName;
        this.date = date;
        this.pdfUrl = pdfUrl;
    }

    public String geteDocumentID() {
        return eDocumentID;
    }

    public void seteDocumentID(String eDocumentID) {
        this.eDocumentID = eDocumentID;
    }

    public String geteDocumentDescription() {
        return eDocumentDescription;
    }

    public void seteDocumentDescription(String eDocumentDescription) {
        this.eDocumentDescription = eDocumentDescription;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }
}
